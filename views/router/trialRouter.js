var app = angular.module("myApp", ["ngRoute", "controllers"]);
app.config(function($routeProvider){
    $routeProvider
        .when("/1", {
            templateUrl: "/view/userselect.html",
            controller: "userCtrl"
        })
        .when("/2", {
            templateUrl: "/view/title.html",
            controller: "trialCtrl"
        })
        .when("/3", {
            templateUrl: "/view/a.html",
            controller: "trialCtrl"
        })
        .when("/4", {
            templateUrl: "/view/a.html",
            controller: "trialCtrl"
        })
        .when("/5", {
            templateUrl: "/view/b.html",
            controller: "trialCtrl"
        })
        .when("/6", {
            templateUrl: "/view/b.html",
            controller: "trialCtrl"
        })
        .when("/7", {
            templateUrl: "/view/choice.html",
            controller: "trialCtrl"
        })
        .when("/8", {
            template: "<h1>Press F10 to continue</h1>",
            controller: "trialCtrl"
        })
        .when("/home", {
            template: "<h1>HOME</h1>",
            controller: "homeCtrl"
        })
        .otherwise({
            redirectTo: "/1"
        });
    });
    
app.controller('trialController', function($scope, $rootScope, $http){
    $(function(){
        $rootScope.page = parseInt(location.hash.split('/')[1]);
       
        $scope.pageChange = function(change){
            if(!$rootScope.user){
                alert("Please select a user!");
            }
            else{
                if($rootScope.page === 7){
                    $scope.saveData();
                }

                $rootScope.page = $rootScope.page + change;
                window.location.hash = '#!' + $rootScope.page;
            }
        };
        $scope.pageChange(0);
       
        $rootScope.initialize = function(){
            $rootScope.experiment_result = {
                user: $rootScope.user,
                part: '',
                agree: '',
                reason: '',
                emotionA: '',
                emotionB: ''
            };
        };
        
        $rootScope.initialize();
       
        $scope.saveData = function(){
            $http.get('/result?id='+$rootScope.experiment_result.user+"&part="+$rootScope.experiment_result.part)
                    .then(function(result){
                        console.log("Getted result: ", result);
                        if(result.data.length === 0){
                            console.log('posting');
                            $http.post(url = '/result',
                                        body = JSON.stringify($rootScope.experiment_result))
                                                .then(function(response){
                                                    console.log(response);
                                                    $rootScope.initialize();
                                        });
                        }
                        else{
                            console.log('putting');
                            $http.put(url = '/result',
                                        body = JSON.stringify($rootScope.experiment_result))
                                                .then(function(response){
                                                    console.log(response);
                                                    $rootScope.initialize();
                                        });
                        }
                    });
        };
   }); 
});

