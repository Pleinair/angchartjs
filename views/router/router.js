var app = angular.module("myApp", ["ngRoute", "controllers"]);
app.config(function($routeProvider){
    $routeProvider
        .when("/1", {
            templateUrl: "/view/userselect.html",
            controller: "userCtrl"
        })
        .when("/2", {
            templateUrl: "/view/title.html",
            controller: "nizrulCtrl"
        })
        .when("/3", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/4", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/5", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/6", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/7", {
            templateUrl: "/view/choice.html",
            controller: "nizrulCtrl"
        })
        .when("/8", {
            templateUrl: "/view/title.html",
            controller: "nizrulCtrl"
        })
        .when("/9", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/10", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/11", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/12", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/13", {
            templateUrl: "/view/choice.html",
            controller: "nizrulCtrl"
        })
        .when("/14", {
            templateUrl: "/view/title.html",
            controller: "nizrulCtrl"
        })
        .when("/15", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/16", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/17", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/18", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/19", {
            templateUrl: "/view/choice.html",
            controller: "nizrulCtrl"
        })
        .when("/20", {
            template: "<h1>Press F10 to continue</h1>",
            controller: "hilmanCtrl"
        })
        .when("/home", {
            template: "<h1>HOME</h1>",
            controller: "homeCtrl"
        })
        .otherwise({
            redirectTo: "/1"
        });
    });
    
app.controller('mainController', function($scope, $rootScope, $http){
    $(function(){
        $rootScope.page = parseInt(location.hash.split('/')[1]);
       
        $scope.pageChange = function(change){
            if(!$rootScope.user){
                alert("Please select a user!");
            }
            else{
                if($rootScope.page === 7 || $rootScope.page === 13 || $rootScope.page === 19){
                    $scope.saveData();
                }

                $rootScope.page = $rootScope.page + change;
                window.location.hash = '#!' + $rootScope.page;
            }
        };
        $scope.pageChange(0);
       
        $rootScope.initialize = function(){
            $rootScope.experiment_result = {
                user: $rootScope.user,
                part: '',
                agree: '',
                reason: '',
                emotionA: '',
                emotionB: ''
            };
        };
        
        $rootScope.initialize();
       
        $scope.saveData = function(){
            $http.get('/result?id='+$rootScope.experiment_result.user+"&part="+$rootScope.experiment_result.part)
                    .then(function(result){
                        var res = result.data[0];
                        console.log("Getted result: ", res);
                        if(result.data.length === 0){
                            console.log('posting');
                            $http.post(url = '/result',
                                        body = JSON.stringify($rootScope.experiment_result))
                                                .then(function(response){
                                                    console.log(response);
                                                    $rootScope.initialize();
                                        });
                        }
                        else{
                            if(!$rootScope.experiment_result.emotionA){
                                $rootScope.experiment_result.emotionA = res.exp_emotion_a;
                            }
                            if(!$rootScope.experiment_result.emotionB){
                                $rootScope.experiment_result.emotionB = res.exp_emotion_b;
                            }
                            if(!$rootScope.experiment_result.reason){
                                $rootScope.experiment_result.reason = res.exp_reason;
                            }
                            if(!$rootScope.experiment_result.agree){
                                $rootScope.experiment_result.agree = res.exp_agreement;
                            }
                            console.log('putting: ', $rootScope.experiment_result);
                            $http.put(url = '/result',
                                        body = JSON.stringify($rootScope.experiment_result))
                                                .then(function(response){
                                                    console.log(response);
                                                    $rootScope.initialize();
                                        });
                        }
                    });
        };
   }); 
});

