/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var dir = angular.module('directives', []);

dir.directive('chartDir', function($parse){
    return {
        restrict: 'E',
        scope: true,
        compile: function(tElement, tAttribute){
            var canvas = '<div><canvas id="' + tAttribute.chartId + '"></canvas><div id="' + tAttribute.inputId + '"></div></div>';
            tElement.html(canvas);
        
            return {
                pre: function(scope, iElement, iAttributes){
                    var vm = scope.chartDirCtrl;
                },
                post: function(scope, iElement, iAttributes){
                    var vm = scope.chartDirCtrl;
                    console.log('from post:', vm.chart);
                    // vm.eventList.watchChartData(vm.chartValues);
                    if(iAttributes.inputId){
                        vm.generateInput();
                        vm.eventList.inputAreaOnChange();
                    }
                }
            };
        },
        controllerAs: 'chartDirCtrl',
        controller: function($scope, $element, $attrs){
            var vm = this;
            
            initialize();
            
            function initialize(){
                vm.chartType = 'line';
                vm.data = {};
                vm.options = {};
                vm.createChart = createChart;
                vm.generateInput = generateInput;
                vm.eventList = eventList();
                parseInput();
                vm.chart = createChart($('#'+vm.chartId));
                vm.chartValues = vm.chart.data.datasets[0].data;
                console.log('from controller:', vm.chart.data);
                console.log($element.find('#'+vm.inputId));
                console.log($('#'+vm.inputId));
            }
            
            function parseInput(){
                if($attrs.chartId) {vm.chartId = $attrs.chartId;}
                if($attrs.chartType) {vm.chartType = $attrs.chartType;}
                if($attrs.inputId) {vm.inputId = $attrs.inputId;}
                if($attrs.data) {vm.data = $parse($attrs.data)($scope.$parent);}
                if($attrs.options) {vm.options = $parse($attrs.data)($scope.$parent);}
            }
            
            function eventList(){
                return {
                    'watchChartData': function(data){ // currently unused because 
                        console.log('from watcher init:', data);
                        $scope.$watchCollection(angular.bind(vm, function(){return vm.chartValues;}) , function(newValue, oldValue){
                            console.log('from watcher:', data, oldValue, newValue);
                            if(JSON.stringify(oldValue) !== JSON.stringify(newValue)){
                                vm.chart.data.datasets[0].data = newValue;
                                vm.chart.update();
                            }
                        }, true);
                    },
                    
                    'inputAreaOnChange': function(){
                        $('#'+vm.inputId).on('change', function(){
                            target = $(event.target);
                            if(target.is('input')){
                                index = target.attr('id').split('-')[1];
                                // alert('updating manually! ' + index);
                                array = vm.chartValues;
                                value = event.target.value;
                                array[index] = parseInt(value);
                                updateChart();
                            }
                        });
                    }
                };
            }
            
            function createChart(ctx){
                console.log({...{type: vm.chartType}, ...{data: vm.data}, ...{options: vm.options}});
                return new Chart(ctx, {...{type: vm.chartType}, ...{data: vm.data}, ...{options: vm.options}});
            }
            
            function generateInput(){
                var data = vm.chartValues;
                inputGroup = document.getElementById(vm.inputId);
                for(i = 0; i < data.length; i++){
                    input = '<input id="data-' + i 
                            + '" type="number" ' 
                            + 'value="' + data[i] 
                            + '"><br>';
//                    input = document.createElement('input');
//                    input.id = 'data-' + i;
//                    input.type = 'number';
//                    input.value = data[i];
                    inputGroup.innerHTML = inputGroup.innerHTML + input;
                    // input.addEventListener('change', updateManual(i), true);
                    // $('#data-' + i).on('blur', vm.updateManual);
                }
            }
            
            function updateChart(){
                vm.chart.update();
            }
        }
    };
});

