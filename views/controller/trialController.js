if(!ctrl){
    var ctrl = angular.module("controllers", []);
}
ctrl.controller("trialCtrl", ['$scope','$rootScope','$http', function ($scope, $rootScope, $http) {
    $(function(){        
        if($rootScope.page>=2 && $rootScope.page<=7){
            var aValue = $rootScope.page - 2;
            var bValue = $rootScope.page - 4;
            if($rootScope.page >2)
                $rootScope.experiment_result.part = 'trial';
            if(!$scope.content){
                $http.get('/content?topic=trial')
                    .then(function(response){
                        $scope.content=response.data;
                        $scope.subtitle = $scope.content.title.split('(')[1].split(')')[0];
                        $scope.a = $scope.content['a' + aValue];
                        $scope.b = $scope.content['b' + bValue];
                });
            }
        }
        
        $scope.choiceClick = function(selection, event){
            $rootScope.experiment_result.agree = selection;
            var choices = $('.choice');
            for(let x of choices){
                $(x).removeClass('choiceActive');
                $(x).find('img').attr('src', $(x).find('img').attr('src').replace('White', 'Grey'));
            };
            $(event.target).find('img').attr('src', $(event.target).find('img').attr('src').replace('Grey', 'White'));
            $(event.target).addClass('choiceActive');
        };
        
        $scope.changeImageColor = function(src, event){
            if(!$(event.target).hasClass('choiceActive')){
                $(event.target).find('img').attr('src', src);
            }
        };
        
        $('#emotionA').find('div').on("click", function(event){
            var allDivs = $('#emotionA').find('div');
            for(let div of allDivs){
                $(div).removeClass('choiceActive');
            }
            $(event.target).addClass('choiceActive');
            $rootScope.experiment_result.emotionA = $(event.target).text();
        });
        
        $('#emotionB').find('div').on("click", function(event){
            var allDivs = $('#emotionB').find('div');
            for(let div of allDivs){
                $(div).removeClass('choiceActive');
            }
            $(event.target).addClass('choiceActive');
            $rootScope.experiment_result.emotionB = $(event.target).text();
        });
        
        $('#reason').on("change", function(event){
            console.log(event.target, $scope.value);
            $rootScope.experiment_result.reason = $scope.value.replace("'", ""); 
        });
        
        console.log($rootScope.experiment_result);
        
        if(!$rootScope.experiment_result){
            $rootScope.initialize();
            console.log('initialized');
        };
    });
}]);

