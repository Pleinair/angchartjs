if(!ctrl){
    var ctrl = angular.module("controllers", []);
}
ctrl.controller("userCtrl", ['$scope','$rootScope','$http', function ($scope, $rootScope, $http) {
        $scope.userlist = [];
        $(function(){
            $http.get('/userlist')
                    .then(function(response){
                        console.log(response);
                        $scope.userlist = response.data;
            });
            
            $scope.selectUser = function(id, event){
                var allUsers = $('.user');
                for(let user of allUsers){
                    $(user).removeClass('choiceActive');
                }
                $rootScope.user = id;
                $rootScope.initialize();
                $(event.target).addClass('choiceActive');
            };
        });
}]);
