/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ctrl = angular.module('chartApp', ['chart.js', 'routing', 'directives']);

ctrl.controller('chartCtrl', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http){
    var vm = this;
    vm.chart = '';
    vm.message = 'The following is a test chart';
    vm.createChart = createChart;
    vm.angular = true;
    vm.data = [10,20,30,40,50,70];
    vm.update = update;
    vm.updateManual = updateManual;
    vm.initManual = initManual;
    vm.testData = {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            };
    vm.testOptions = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            };
    vm.chartSettings = {
        data: [10,40,30,40,70],
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
        series: ['Series A', 'Series B'],
        options: {
            scales: {
                yAxes: [{
                        id: 'y-axis',
                        type: 'linear',
                        display: true,
                        position: 'left'
                        },
                        {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                        }
                        ]
            }
        },
        datasetOverride: [{ yAxisID: 'y-axis-1'}, { yAxisID: 'y-axis-2'}]
    };
    // chart = createChart('line', vm.chartSettings);
    // document.getElementById('chartParent').appendChild(chart);
    initManual();
    
    
    function createChart(type, settings){
        var chart = document.createElement('canvas');
        for (y in settings){
            console.log(y);
        }
        chart.setAttribute('class', 'chart chart-' + type);
        for (let setting in settings){
            settingName = setting;
            for (let x of setting){
                if(x === x.toUpperCase()){
                    settingName = setting.split(x).join('-'+x.toLowerCase());
                }
            }
            chart.setAttribute('chart-' + settingName, 'vm.chartSettings.'+setting);
        }
        return chart;
    }
    
    function initManual(check){
        if(!(check || vm.chart)){
            manualChart();
        }
    }
    
    function manualChart(){
        var ctx = document.getElementById('myChart').getContext('2d');
        data = [12,19,3,5,2,3];
        vm.chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        generateInput(data);
    }
    
    function generateInput(data){
        console.log(data, vm.chart.data);
        inputGroup = document.getElementById('chartInput');
        for(i = 0; i < data.length; i++){
            input = document.createElement('input');
            input.id = 'data-' + i;
            input.type = 'number';
            input.value = data[i];
            inputGroup.appendChild(input);
            // input.addEventListener('change', updateManual(i), true);
            $('#data-' + i).on('blur', vm.updateManual);
        }
        //$('#chartInput').on('blur', updateManual());
    }
    
    function updateManual(){
        target = $(event.target);
        console.log(event.target);
        if(target.is('input')){
            console.log(target.attr('id'));
            index = target.attr('id').split('-')[1];
            // alert('updating manually! ' + index);
            array = vm.chart.data.datasets[0].data;
            //value = document.getElementById('data-' + index).value;
            value = event.target.value;
            array[index] = value;
            vm.chart.update();
        }
    }
    
    function update(index, value){
        temp = vm.data;
        temp[index] = value;
        vm.data = temp;
    }
}]);


