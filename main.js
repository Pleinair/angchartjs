/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global __dirname */

var fs = require('fs');
var http = require('http');
var url = require('url');
var util = require("util");
var express = require('express'); 
var cors = require('cors');
var path = require('path');

//connection to database
var pg = require('pg');
var conString = "postgres://postgres:postgres@localhost:5432/ExperimentDB";

var client = new pg.Client(conString);
// client.connect();
//end of connection to database

cors({credentials: true, origin: true});

var app = express();
app.use(cors());
app.use(express.urlencoded());
app.use(express.json());
app.use('/js', express.static(__dirname + '/js'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));
app.use('/dist', express.static(__dirname + '/../dist'));
app.use('/img', express.static(__dirname + '/public/images'));
app.use('/imgA', express.static(__dirname + '/public/hilmanImagesA'));
app.use('/imgB', express.static(__dirname + '/public/hilmanImagesB'));
app.use('/css', express.static(__dirname + '/css'));
app.use('/partials', express.static(__dirname + '/partials'));
app.use('/controller', express.static(__dirname + '/views/controller'));
app.use('/directive', express.static(__dirname + '/views/directive'));
app.use('/router', express.static(__dirname + '/views/router'));
app.use('/view', express.static(__dirname + '/views'));

app.route('/web/*').get((req, res) => {
    res.sendFile(path.resolve(__dirname + '/views/index.html'));
});

app.route('/chart').get((req, res) => {
    res.sendFile(path.resolve(__dirname + '/views/chart.html'));
});

app.route('/trial').get((req, res) => {
    res.sendFile(path.resolve(__dirname + '/views/trial.html'));
});

app.listen(1010, function(){
    console.log("Hey it's actually a page for angular and chart js");
});

